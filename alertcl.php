<?php
/*
Plugin Name: Alert CL
Plugin URI: http://www.chileayuda.com
Description: Watch all disasters arround the World
Version: 0.1
Author: José Espina, Jorge Álvarez
Author URI: http://www.imatec.cl
License: Ocúpelo quien quiera.
*/
?>
<?php



$disaster_types = array(
    1 => 'Tsunami',
    2 => 'Terremoto',
    3 => 'Huracán',
    4 => 'Volcán',
);

//add_action('admin_init', 'my_plugin_admin_init');
add_action('admin_menu', 'my_plugin_admin_menu');

    function my_plugin_admin_menu()
    {
        /* Register our plugin page */
        //add_menu_page($page_title, $menu_title, $access_level, $file, $function, $icon_url, $position)
        $page = add_menu_page( 'alertcl',
                               'alertcl',
                               'administrator',
                               'my_plugin_manage_menu',
                               'my_plugin_manage_menu');
//        $editPollPage = add_submenu_page( 'Agregar Feed',
//                                          'Agregar Feed',
//                                          'administrator',
//                                          'add_feed',
//                                          'add_feed');
    }

    function my_plugin_manage_menu()
    {
        global $disaster_types;
        include("../wp-content/plugins/alertcl/admin.php");
        /* Output our admin page */
    }

//    function add_feed(){
//        echo "dajfnajkfbahsdbfsadjfbsa";
//    }

function myplugin_activate() {

    global $wpdb;
    $table_name = $wpdb->prefix."alertcl_rss";

    $sql = "CREATE TABLE " . $table_name . " (
	  id mediumint(9) NOT NULL AUTO_INCREMENT,
	  type tinyint(1) DEFAULT '1' NOT NULL,
	  url VARCHAR(255) NOT NULL,
          activated tinyint(1) DEFAULT '1' NOT NULL,
	  UNIQUE KEY id (id)
	);";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);


    //Tsunami, Global y por continente
    $wpdb->insert($wpdb->prefix."alertcl_rss",array('type' => 1,'url' => 'http://wcatwc.arh.noaa.gov/rss/tsunamirss.xml', 'activated' => 0),array( '%d', '%s', '%d' ));
    $wpdb->insert($wpdb->prefix."alertcl_rss",array('type' => 1,'url' => 'http://www.prh.noaa.gov/ptwc/feeds/ptwc_rss_caribe.xml', 'activated' => 0),array( '%d', '%s', '%d' ));
    $wpdb->insert($wpdb->prefix."alertcl_rss",array('type' => 1,'url' => 'http://www.prh.noaa.gov/ptwc/feeds/ptwc_rss_indian.xml', 'activated' => 0),array( '%d', '%s', '%d' ));
    $wpdb->insert($wpdb->prefix."alertcl_rss",array('type' => 1,'url' => 'http://www.prh.noaa.gov/ptwc/feeds/ptwc_rss_hawaii.xml', 'activated' => 0),array( '%d', '%s', '%d' ));
    $wpdb->insert($wpdb->prefix."alertcl_rss",array('type' => 1,'url' => 'http://www.prh.noaa.gov/ptwc/feeds/ptwc_rss_pacific.xml', 'activated' => 0),array( '%d', '%s', '%d' ));

    //Terremoto Global
    $wpdb->insert($wpdb->prefix."alertcl_rss",array('type' => 2,'url' => 'http://earthquake.usgs.gov/earthquakes/catalogs/1day-M2.5.xml', 'activated' => 0),array( '%d', '%s', '%d' ));
    $wpdb->insert($wpdb->prefix."alertcl_rss",array('type' => 2,'url' => 'http://earthquake.usgs.gov/eqcenter/catalogs/eqs1day-M2.5.xml', 'activated' => 0),array( '%d', '%s', '%d' ));
    $wpdb->insert($wpdb->prefix."alertcl_rss",array('type' => 2,'url' => 'http://earthquake.usgs.gov/earthquakes/catalogs/7day-M2.5.xml', 'activated' => 0),array( '%d', '%s', '%d' ));
    $wpdb->insert($wpdb->prefix."alertcl_rss",array('type' => 2,'url' => 'http://earthquake.usgs.gov/earthquakes/catalogs/7day-M5.xml', 'activated' => 0),array( '%d', '%s', '%d' ));

    //Huracanes, Tornados
    //Volcanes
    $wpdb->insert($wpdb->prefix."alertcl_rss",array('type' => 4,'url' => 'http  ://volcano.wr.usgs.gov/rss/vhpcaprss.xml', 'activated' => 0),array( '%d', '%s', '%d' ));
 
}

register_activation_hook( __FILE__, 'myplugin_activate' );
function load_alert_cl(){

    function alert_cl(){
        include(ABSPATH . 'wp-content/plugins/alertcl/rsslib/rsslib.php');        
      //  return RSS_Display('http://wcatwc.arh.noaa.gov/rss/tsunamirss.xml');
        return RSS_Display('http://rss.nyalert.gov/RSS/CapIndices/_NewYorkStateRSSCAPIndex.xml');
    }

}


add_action('wp_head', 'load_alert_cl');
add_action('', 'initial_rss_feeds');

?>